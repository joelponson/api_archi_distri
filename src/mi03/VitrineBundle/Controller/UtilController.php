<?php
namespace mi03\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use mi03\VitrineBundle\Entity\Panier;
use mi03\VitrineBundle\Entity\LigneCommande;
use Symfony\Component\HttpFoundation\Request;

class UtilController extends Controller
{
    public function ArticlesPanierAction()
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $panier = array();
        $lePanier = $session->get('panier', new Panier());
        if ($lePanier->getContenu() != null)
        {
            foreach ($lePanier->getContenu() as $id_article => $quantite)
            {
                $article = $em->getRepository('VitrineBundle:Article')->find($id_article);
                array_push($panier, array('article'=>$article, 'quantite'=>$quantite));
            }
        }
        return $this->render('mi03VitrineBundle:eSide:articlesPanier.html.twig', array('articlesPanier' => $panier));
    }
    
    public function infosClientAction()
    {
        $user = $this->getUser();
        if ($user != null)
        {
            $em = $this->getDoctrine()->getManager();
            $client = $em->getRepository('mi03VitrineBundle:Client')->find($user->getId());
            return $this->render('mi03VitrineBundle:eSide:infosClient.html.twig', array('client' => $client));
        }
        return $this->render('mi03VitrineBundle:eSide:empty_block.html.twig');
    }
    
    public function plusVendusAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository('mi03VitrineBundle:LigneCommande')->getArticlesPlusVendus();
        return $this->render('mi03VitrineBundle:eSide:articlesPlusVendus.html.twig', array('plusVendus' => $articles));
    }
    
    public function articlesRuptureAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository('mi03VitrineBundle:Article')->getArticlesRupture();
        return $this->render('mi03VitrineBundle:eSide:articlesRupture.html.twig', array('articles' => $articles));
    }
    
    public function articlesConseillesAction($id_article)
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository('mi03VitrineBundle:LigneCommande')->getArticlesConseillés($id_article);
        return $this->render('mi03VitrineBundle:eSide:articlesConseilles.html.twig', array('articles' => $articles));
    }
    public function ArticleAction($articleId)
    {
        $article = $this->getDoctrine()->getManager()
            ->getRepository('mi03VitrineBundle:Article')
            ->find($articleId);


        if (!$article) {
            throw $this->createNotFoundException('Produit non trouvé !');
        }

        return $this->render('mi03VitrineBundle:Default:article.html.twig', array('current_menu' => 'catalogue', 'article' => $article));
    }
    public function ajouterAction($articleId, $qte, Request $request)
    {
        $session = $request->getSession();

        if (!$session->has('panier')) {
            $panier = $session->set('panier', new Panier());
        } else {
            $panier = $session->get('panier');
            $session->set('panier', $panier);
        }

        if (array_key_exists($articleId,$panier)) {

            if ($request->query->get('qte') != null){
                $panier[$articleId]= $panier[$articleId] + $request->query->get('qte');
            }

        } else {
            if ($request->query->get('qte') != null){
                $panier->ajoutArticle($articleId, $request->query->get('qte'));
            }else{
                $panier->ajoutArticle($articleId, $qte);
            }

        }
        $session->set('panier', $panier);
        $panier = $session->get('panier');
        return $this->redirect($this->generateUrl('panier_contenuPanier'));
    }

}

