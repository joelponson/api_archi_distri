<?php

namespace mi03\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CatalogueController extends Controller {

    public function indexAction($name) {
        
        return $this->render("mi03VitrineBundle:Default:index.html.twig",array('name' => "$name"));
        
    }

    public function mentionsAction() {
        return $this->render("mi03VitrineBundle:Default:mentions.html.twig");
    }

    public function catalogueAction() {

        $categories = $this->getDoctrine()->getManager()
                ->getRepository('mi03VitrineBundle:Categorie')
                ->findAll();
        if (!$categories) {
            throw $this->createNotFoundException('aucune categorie trouvée avec id ' . $id);
        }
        return $this->render("mi03VitrineBundle:Catalogue:catalogue.html.twig", array('categories' => $categories));
    }

}
