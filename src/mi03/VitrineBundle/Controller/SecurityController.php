<?php
namespace mi03\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;



class SecurityController extends Controller {
    public function loginAction() {
        $authenticationUtils = $this->get('security.authentication_utils');
        // on récupère l’erreur de login s’il y en avait une
        $error = $authenticationUtils->getLastAuthenticationError();
        // on récupère le dernier login saisi par l’utilisateur, s’il y en avait un
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('mi03VitrineBundle:Security:login.html.twig', array(
 'last_username' => $lastUsername,
 'error' => $error,
 ));
 }

}