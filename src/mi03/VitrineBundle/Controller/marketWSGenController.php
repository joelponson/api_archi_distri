<?php
namespace mi03\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Zend\Soap\AutoDiscover;

class marketWSGenController extends AbstractController
{
    public function marketGenAction()
    {
        $autodiscover = new AutoDiscover();
        $service = $this->get('service_soap');
        $autodiscover->setClass($service)
            ->setUri('http://localhost:8000/soap');

        header('content-Type : application/wsdl+xml');
        $wsdl = $autodiscover->generate();
        $wsdl->dump('../marketWS.wsdl');

        return new Response($wsdl->toXml());
    }
}
