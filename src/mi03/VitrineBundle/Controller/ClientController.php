<?php

namespace mi03\VitrineBundle\Controller;

use mi03\VitrineBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Client controller.
 *
 */
class ClientController extends Controller
{
    /**
     * Lists all client entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('mi03VitrineBundle:Client')->findAll();

        return $this->render('client/index.html.twig', array(
            'clients' => $clients,
        ));
    }
    public function authentificationAction(Request $request)
    {
        $user = $this->getUser();
        if ($user != null)
        {
            $em = $this->getDoctrine()->getManager();
            $client = $em->getRepository('mi03VitrineBundle:Client')->find($user->getId());
            $session = $this->getRequest()->getSession();
            if ($session->get('action') === "validationPanier")
            {
                return $this->redirect($this->generateUrl('client_validationPanier'));
            }
            return $this->render('client/monCompte.html.twig',array('client' => $client));
        }
        return $this->render('mi03VitrineBundle:Default:authentification.html.twig');
    }
    public function monCompteAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('mi03VitrineBundle:Client')->find($user->getId());
        return $this->render('client/monCompte.html.twig',array('client' => $client));

        if ($user != null)
        {

            if ($session->get('action') === "validationPanier")
            {
                return $this->redirect($this->generateUrl('client_validationPanier'));
            }
            return $this->render('mi03/VitrineBundle:Default:monCompte.html.twig',array('client' => $client));
        }
        return $this->render('mi03VitrineBundle:Default:authentification.html.twig');
    }
    /**
     * Creates a new client entity.
     *
     */
    public function newAction(Request $request)
    {
        $client = new Client();
        $form = $this->createForm('mi03\VitrineBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->container->get('security.password_encoder');
            // On récupère l'encodeur défini dans security.yml
            $encoded = $encoder->encodePassword($client, $client->getPassword());
            // On encode le mot de passe issu du formulaire
            $client->setMdp($encoded);
            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('client_show', array('id' => $client->getId()));
        }

        return $this->render('client/new.html.twig', array(
            'client' => $client,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a client entity.
     *
     */
    public function showAction(Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);

        return $this->render('client/show.html.twig', array(
            'client' => $client,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing client entity.
     *
     */
    public function editAction(Request $request, Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);
        $editForm = $this->createForm('mi03\VitrineBundle\Form\ClientType', $client);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('client_edit', array('id' => $client->getId()));
        }

        return $this->render('client/edit.html.twig', array(
            'client' => $client,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a client entity.
     *
     */
    public function deleteAction(Request $request, Client $client)
    {
        $form = $this->createDeleteForm($client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
        }


        return $this->redirectToRoute('client_index');
    }

    /**
     * Creates a form to delete a client entity.
     *
     * @param Client $client The client entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Client $client)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('client_delete', array('id' => $client->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
