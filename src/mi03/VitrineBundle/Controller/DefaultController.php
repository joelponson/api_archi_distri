<?php

namespace mi03\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

    public function indexAction() {
        
        return $this->render("mi03VitrineBundle:Default:index.html.twig");
        
    }

    public function mentionsAction() {
        return $this->render("mi03VitrineBundle:Default:mentions.html.twig");
    }

    public function catalogueAction() {

        $categories = $this->getDoctrine()->getManager()
                ->getRepository('mi03VitrineBundle:Categorie')
                ->findAll();
        if (!$categories) {
            throw $this->createNotFoundException('aucune categorie trouvée avec id ' );
        }
        return $this->render("mi03VitrineBundle:Default:catalogue.html.twig", array('categories' => $categories));
    }
    public function ArticlesAction() {

        $articles = $this->getDoctrine()->getManager()
            ->getRepository('mi03VitrineBundle:Article')
            ->findAll();
        if (!$articles) {
            throw $this->createNotFoundException('aucun Article trouvée  ' );
        }
        return $this->render("mi03VitrineBundle:Default:articles.html.twig", array('articles' => $articles));
    }


    public function articlesParCategorieAction($id_categorie) {

                $em = $this->getDoctrine()->getManager();
                $categorie = $em->getRepository('mi03VitrineBundle:Categorie')
                    ->find($id_categorie);
        if (!$categorie)
            throw $this->createNotFoundException('La catégorie n\'existe pas');

        $articlesDeCat = $categorie->getArticles();

        if (!$articlesDeCat) {
            throw $this->createNotFoundException('aucun article dans cette categorie  ' );
        }
        return $this->render("mi03VitrineBundle:Default:articlesParCategorie.html.twig", array('articles' => $articlesDeCat, 'categorie' => $categorie));
    }
    public function backOfficeAction()
    {
        return $this->render('mi03VitrineBundle:BackOffice:index.html.twig');
    }
}
