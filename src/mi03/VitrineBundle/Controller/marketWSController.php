<?php
namespace mi03\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class marketWSController extends AbstractController
{
    public function marketWSAction()
    {   
        ini_set("soap.wsdl_cache_enabled", "0");
        $option= array(
            'trace'=>1, 
            'encoding'  => 'UTF-8',
            'uri' => 'http://127.0.0.1:8000/soap',
            'cache_wsdl' => WSDL_CACHE_NONE, 
            'exceptions' => true
        );

        $service = $this->get('service_soap');
        $soapServer = new \SoapServer('..\marketWS.wsdl', $option);
        $soapServer->setObject($service);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=UTF-8');

        ob_start();
        $soapServer->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }
}
