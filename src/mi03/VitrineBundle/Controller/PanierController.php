<?php

namespace mi03\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use mi03\VitrineBundle\Entity\Panier;

class PanierController extends Controller
{
    public function contenuPanierAction( Request $request)
    {
        $session = $this->get('session');
        //$session = $request->getSession();
        $lePanier = $session->get('panier', new Panier());
       $servicepanier = $this->get('service_panier');
       $contenuPanier =$servicepanier->contenuPanier($lePanier);

        return $this->render('mi03VitrineBundle:Panier:contenuPanier.html.twig', $contenuPanier);

    }
    public function validerPanierAction( Request $request)
    {
        $user = $this->getUser();
        if ($user != null) {
            $session = $this->get('session');
            $lePanier = $session->get('panier', new Panier());
            $servicepanier = $this->get('service_panier');

            $commande = $servicepanier->convertirCommande($lePanier, $user->getId());
            $session = $request->getSession();
            $session->set('panier', new Panier());
            return $this->redirect($this->generateUrl('panier_commande'));
        }
        else{
            return $this->redirect($this->generateUrl('client_authentification'));
        }
    }
    public function panierCommandeAction( Request $request)
    {

        return $this->render('mi03VitrineBundle:Panier:panierValide.html.twig');

    }


    public function ArticlesPanierAction( Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $panier = array();
        $lePanier = $session->get('panier', new Panier());
        if ($lePanier->getContenu() != null)
        {
            foreach ($lePanier->getContenu() as $id_article => $quantite)
            {
                $article = $em->getRepository('mi03VitrineBundle:Article')->find($id_article);
                array_push($panier, array('article'=>$article, 'quantite'=>$quantite));
            }
        }
        return $this->render('mi03VitrineBundle:eSide:articlesPanier.html.twig', array('articlesPanier' => $panier));
    }

    public function ajoutArticleAction($id_article, $quantite, Request $request)
    {
        $session = $request->getSession();

            $lePanier = $session->get('panier', new Panier());


        $lePanier->ajoutArticle($id_article, $quantite);
        $session->set('panier', $lePanier);
        $lePanier = $session->get('panier');      
        return $this->redirect($this->generateUrl('panier_contenuPanier'));
    }
    
    public function moreArticleAction($id_article, Request $request)
    {
        $session = $request->getSession();
        $lePanier = $session->get('panier', new Panier());
        $lePanier->moreArticle($id_article);
        $session->set('panier', $lePanier);
        $lePanier = $session->get('panier');
        return $this->redirect($this->generateUrl('panier_contenuPanier'));
    }
    
    public function lessArticleAction($id_article, Request $request)
    {
        $session = $request->getSession();
        $lePanier = $session->get('panier', new Panier());
        $lePanier->lessArticle($id_article);
        $session->set('panier', $lePanier);
        $lePanier = $session->get('panier');
        return $this->redirect($this->generateUrl('panier_contenuPanier'));
    }
    
    public function suppressionArticleAction($id_article, Request $request)
    {
        $session = $request->getSession();
        $lePanier = $session->get('panier', new Panier());
        $lePanier->supprimerArticle($id_article);
        $session->set('panier', $lePanier);
        $lePanier = $session->get('panier');
        return $this->redirect($this->generateUrl('panier_contenuPanier'));
    }
    
    public function viderPanierAction( Request $request)
{
    $session = $request->getSession();
    $session->set('panier', new Panier());
    return $this->redirect($this->generateUrl('panier_contenuPanier'));
}



}

