<?php

# Service/ServicePanier.php
namespace mi03\VitrineBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use mi03\VitrineBundle\Entity\Panier;
use mi03\VitrineBundle\Entity\Commande;
use mi03\VitrineBundle\Entity\LigneCommande;
use Doctrine\ORM\EntityManager;

class ServicePanier
{
    private $montant;
    private $entmana;

    /**
     * @return EntityManager
     */
    public function getEntmana()
    {
        return $this->entmana;
    }

    /**
     * @param EntityManager $entmana
     */
    public function setEntmana($entmana)
    {
        $this->entmana = $entmana;
    }

    public function __construct(EntityManager $em) {
        $this->entmana = $em;
    }
    public function getMontant() {
        return $this->montant;
    }
    public function setMontant($mont) {
         $this->montant = $mont;
    }
    public function contenuPanier($lePanier)
    {
        // ***** Creation Tableau d'objet pour la vue *****
        $em = $this->getEntmana();
        $panier = array();
        $total = array('nbArticles'=>0, 'total'=>0);
        if ($lePanier->getContenu() != null)
        {
            foreach ($lePanier->getContenu() as $id_article => $quantite)
            {
                $article = $em->getRepository('mi03VitrineBundle:Article')->find($id_article);
                array_push($panier, array('article'=>$article, 'quantite'=>$quantite));
                $total['nbArticles'] += $quantite;
                $total['total'] += $article->getPrix()*$quantite;
            }
        }
        return array('panier' => $panier, 'total' => $total);
    }

    public function convertirCommande($lePanier, $clientid)
    {
        // ***** Creation Tableau d'objet pour la vue *****
        $em = $this->getEntmana();

        if ($lePanier->getContenu() != null)
        {
            $commande = new Commande();
            $client = $em->getRepository('mi03VitrineBundle:Client')->find($clientid);
            $commande->setClient($client);
            $commande->setDate(new \DateTime("now") );
            $commande->setEtat(true);

            $em->persist($commande);
            $em->flush();
            foreach ($lePanier->getContenu() as $id_article => $quantite)
            {
                $article = $em->getRepository('mi03VitrineBundle:Article')->find($id_article);

                $lc = new LigneCommande();
                $lc->setArticle($article);
                $lc->setPrix($article->getPrix()*$quantite);
                $lc->setQuantite($quantite);
                $lc->setCommande($commande);
                $em->persist($lc);
                $em->flush();
            }
        }
        return $commande;
    }
}