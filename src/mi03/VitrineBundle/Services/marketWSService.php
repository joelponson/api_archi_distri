<?php

/**
 * Created by PhpStorm.
 * User: LDEB
 * Date: 29/04/2019
 * Time: 13:21
 */

namespace mi03\VitrineBundle\Services;

use Doctrine\ORM\EntityManager;
use mi03\VitrineBundle\Entity\LigneCommande;
use mi03\VitrineBundle\Entity\Article;

class marketWSService
{   
    private $em;

    public function __construct(EntityManager $em)
    {
      $this->em = $em;
    }

    /**
     * Articles les plus vendus
     *
     * @return array()
     *
     */
    public function articlesPlusVendus()
    { 

      $articles = $this->em->getRepository('mi03VitrineBundle:LigneCommande')->getArticlesPlusVendus();

      $array = array();
      $i = 0;

      foreach ($articles as $article)
      {   
          $array[$i]['id'] = $article[0]->getId();
          $array[$i]['nom'] = $article[0]->getNom();
          $array[$i]['prix'] = $article[0]->getPrix();
          $array[$i]['description'] = $article[0]->getDescription();
          $array[$i]['stock'] = $article[0]->getStock();
          $i++;
      }
      return $array;
    }

    /**
     * Articles conseillés
     *
     * @return array()
     *
     */
    public function articlesConseilles($param)
    { 
      $articles = $this->em->getRepository('mi03VitrineBundle:Article')->getArticlesConseillés($param);

      $array = array();
      $i = 0;

      foreach ($articles as $article)
      {   
        $array[$i]['id'] = $article->getId();
        $array[$i]['nom'] = $article->getNom();
        $array[$i]['prix'] = $article->getPrix();
        $array[$i]['description'] = $article->getDescription();
        $array[$i]['stock'] = $article->getStock();
        $i++;
      }

      return $array;
    }

    /**
     * Catégories
     *
     * @return array()
     *
     */
    public function getCategories()
    { 
      $categories = $this->em->getRepository('mi03VitrineBundle:Categorie')->findAll();

      $array = array();
      $i = 0;

      foreach ($categories as $categorie)
      {   
        $array[$i]['id'] = $categorie->getId();
        $array[$i]['nom'] = $categorie->getNom();
        $i++;
      }

      return $array;
    }

    /**
     * Change article
     *
     * @return string()
     * 
     */
    public function updateArticle($forms)
    { 
      if ($forms && (!empty($forms['nom']) || !empty($forms['prix']) || !empty($forms['description']) || !empty($forms['stock']))) {
        $article = $this->em->getRepository('mi03VitrineBundle:Article')->findOneById($forms['id']);
        if (!$article) {
          return "Article non trouvé";
        } else {
          if(!empty($forms['nom'])) {
            $article->setNom($forms['nom']);
          }
          if(!empty($forms['prix'])) {
            $article->setPrix($forms['prix']);
          }
          if(!empty($forms['description'])) {
            $article->setDescription($forms['description']);
          }
          if(!empty($forms['stock'])) {
            $article->setStock($forms['stock']);
          }
          $this->em->persist($article);
          $this->em->flush();
          return "Les données ont bien été mises à jour :)";
        }
      } else {
        return "Rien à modifier !";
      }
    }
}