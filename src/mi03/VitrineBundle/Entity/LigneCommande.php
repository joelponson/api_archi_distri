<?php

namespace mi03\VitrineBundle\Entity;

/**
 * LigneCommande
 */
class LigneCommande
{
    /**
     * @var integer
     */
    private $quantite;

    /**
     * @var string
     */
    private $prix;

    /**
     * @var \mi03\VitrineBundle\Entity\Article
     */
    private $article;

    /**
     * @var \mi03\VitrineBundle\Entity\Commande
     */
    private $commande;


    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return LigneCommande
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return LigneCommande
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set article
     *
     * @param \mi03\VitrineBundle\Entity\Article $article
     *
     * @return LigneCommande
     */
    public function setArticle(\mi03\VitrineBundle\Entity\Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \mi03\VitrineBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set commande
     *
     * @param \mi03\VitrineBundle\Entity\Commande $commande
     *
     * @return LigneCommande
     */
    public function setCommande(\mi03\VitrineBundle\Entity\Commande $commande)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \mi03\VitrineBundle\Entity\Commande
     */
    public function getCommande()
    {
        return $this->commande;
    }
}
