<?php

namespace mi03\VitrineBundle\Entity;

/**
 * Commande
 */
class Commande
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var boolean
     */
    private $etat;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Commande
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat()
    {
        return $this->etat;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lignecommandes;

    /**
     * @var \mi03\VitrineBundle\Entity\Client
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignecommandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lignecommande
     *
     * @param \mi03\VitrineBundle\Entity\LigneCommande $lignecommande
     *
     * @return Commande
     */
    public function addLignecommande(\mi03\VitrineBundle\Entity\LigneCommande $lignecommande)
    {
        $this->lignecommandes[] = $lignecommande;

        return $this;
    }

    /**
     * Remove lignecommande
     *
     * @param \mi03\VitrineBundle\Entity\LigneCommande $lignecommande
     */
    public function removeLignecommande(\mi03\VitrineBundle\Entity\LigneCommande $lignecommande)
    {
        $this->lignecommandes->removeElement($lignecommande);
    }

    /**
     * Get lignecommandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignecommandes()
    {
        return $this->lignecommandes;
    }

    /**
     * Set client
     *
     * @param \mi03\VitrineBundle\Entity\Client $client
     *
     * @return Commande
     */
    public function setClient(\mi03\VitrineBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \mi03\VitrineBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
