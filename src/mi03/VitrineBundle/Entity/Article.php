<?php

namespace mi03\VitrineBundle\Entity;

/**
 * Article
 */
class Article
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prix;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $stock;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lignecommandes;

    /**
     * @var \mi03\VitrineBundle\Entity\Categorie
     */
    private $categorie;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignecommandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Article
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return Article
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Article
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Add lignecommande
     *
     * @param \mi03\VitrineBundle\Entity\LigneCommande $lignecommande
     *
     * @return Article
     */
    public function addLignecommande(\mi03\VitrineBundle\Entity\LigneCommande $lignecommande)
    {
        $this->lignecommandes[] = $lignecommande;

        return $this;
    }

    /**
     * Remove lignecommande
     *
     * @param \mi03\VitrineBundle\Entity\LigneCommande $lignecommande
     */
    public function removeLignecommande(\mi03\VitrineBundle\Entity\LigneCommande $lignecommande)
    {
        $this->lignecommandes->removeElement($lignecommande);
    }

    /**
     * Get lignecommandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignecommandes()
    {
        return $this->lignecommandes;
    }

    /**
     * Set categorie
     *
     * @param \mi03\VitrineBundle\Entity\Categorie $categorie
     *
     * @return Article
     */
    public function setCategorie(\mi03\VitrineBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \mi03\VitrineBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}
