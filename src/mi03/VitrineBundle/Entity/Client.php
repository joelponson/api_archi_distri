<?php

namespace mi03\VitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
class Client implements UserInterface, \Serializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $mail;

    /**
     * @var string
     */
    private $mdp;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Client
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set mdp
     *
     * @param string $mdp
     *
     * @return Client
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get mdp
     *
     * @return string
     */
    public function getMdp()
    {
        return $this->mdp;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commandes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->commandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add commande
     *
     * @param \mi03\VitrineBundle\Entity\Commande $commande
     *
     * @return Client
     */
    public function addCommande(\mi03\VitrineBundle\Entity\Commande $commande)
    {
        $this->commandes[] = $commande;

        return $this;
    }

    /**
     * Remove commande
     *
     * @param \mi03\VitrineBundle\Entity\Commande $commande
     */
    public function removeCommande(\mi03\VitrineBundle\Entity\Commande $commande)
    {
        $this->commandes->removeElement($commande);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandes()
    {
        return $this->commandes;
    }
    public function getUsername() {
        return $this->mail; // l'email est utilisé comme login
    }
    public function getSalt() {
        return null; // inutile avec l’encryptage choisi
    }
    public function getPassword() {
        return $this->mdp;
    }
    public function getRoles() {
        if ($this->isAdministrateur()) // Si le client est administrateur
            return array('ROLE_ADMIN'); // on lui accorde le rôle ADMIN
        else
            return array('ROLE_USER'); // sinon le rôle USER
    }
    public function eraseCredentials(){// rien à faire ici
    }
    public function serialize() { // pour pouvoir sérialiser le Client en session
        return serialize(array($this->id, $this->mail, $this->mdp));
 }
    public function unserialize($serialized) {
        list ($this->id, $this->mail, $this->mdp) = unserialize($serialized);
 }
    public function isAdministrateur(){// rien à faire ici

            return  $this->mail == "didi";

    }

}
