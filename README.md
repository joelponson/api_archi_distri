# TP Noté Web Service

A Symfony project created on February 5, 2019, 1:30 pm.
Create an api on the existing Symfony project.

## Getting Started

Start with cloning the repository of the project

```
git clone https://gitlab.com/joelponson/api_archi_distri.git
```

Go to the project

```
cd api_archi_distri
```
## Deployment

Run composer update

If you have a global installation of composer

```
composer update
```

If not, copy past you composer.phar to the project and launch the command

```
php -d memory_limit=-1 composer.phar install
```

## Congirue Database

### Parameters.yaml

On the composer update, prompt command ask you a choice for some fiels. 

You can push Enter to put the default value. 
But for the database_name you have to enter the following name :

```
mi03
```


### Import database

Go to your localhost/phpmyadmin
Create mi03 database and import data with mi03.sql

## Known Errors

If you have an error with the php memory limit, you have to modify you php.ini, and up the line like that :
memory_limit=2048M


## SOAP

### Require

```
php bin/console server:run
```

### Generate WSDL

Go to : http://127.0.0.1:8000/genwsdl

### See WSDL

Go to : http://127.0.0.1:8000/soap?wsdl

### Lauch client

Put the client.php in your folder of Wamp or Xamp

Go to : http://127.0.0.1/client.php


## API Platform

### Require

```
php bin/console server:run
```

And go to this url : http://127.0.0.1:8000/api
Have fun !