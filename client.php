<style>
table
{
    border-collapse: collapse;
}
td, th /* Mettre une bordure sur les td ET les th */
{
    border: 1px solid black;
}
</style>
<?php
    ini_set("soap.wsdl_cache_enabled", "0");
    $option=array('trace'=>1,'encoding'  => 'UTF-8');
    $soapClient =  new \SoapClient('http://localhost:8000/soap?wsdl', $option);

    try{
        //////////////////////////////////// 
        if (isset($_POST['nom']) || isset($_POST['prix']) ||isset($_POST['description']) || isset($_POST['stock'])) {
            $functionUpdateArticle = 'updateArticle';
            $params = array('id' => $_POST['id'], 'nom' => $_POST['nom'], 'prix' => $_POST['prix'], 'description' => $_POST['description'], 'stock' => $_POST['stock']);
            $result = $soapClient->__soapcall($functionUpdateArticle, array($params));
            echo '<p>'.$result.'</p>';
        }

        ///////////////////////////////////////Get best selling article/////////////////////////////////////////////////////////
        $functionBestSell = 'articlesPlusVendus';
        $bestSell = $soapClient->__soapcall($functionBestSell,[]);
        echo '<h1><b>Artciles les plus vendus : </b></h1><br/>';
        // Show
        echo '<table style="border-collapse: collapse;"><thead><tr><th>Nom</th><th>Prix</th><th>Description</th><th>Stock</th></tr></thead><tbody>';
        foreach ($bestSell as $key => $value) {
            echo '<tr><td>'.$value['nom'].'</td><td>'.$value['prix'].'</td><td>'.$value['description'].'</td><td>'.$value['stock'].'</td></tr>';
        }
        echo '</tbody></table>';
        echo '<br/><br/>';
        
        if (isset($_POST['categorie'])) {
            ////////////////////////////////////Get recommanded articles///////////////////////////////////////////////////////////
            $functionRecommanded = 'articlesConseilles';
            $recommanded = $soapClient->__soapcall($functionRecommanded,['param' => $_POST['categorie']]);
            echo '<h1><b>Artciles que l\'on pourrait vous conseiller  : </b></h1><br/>';
            // dump response
            echo '<table style="border-collapse: collapse;"><thead><tr><th>Nom</th><th>Prix</th><th>Description</th><th>Stock</th></tr></thead><tbody>';
            foreach ($recommanded as $key => $value) {
                echo '<tr><td>'.$value['nom'].'</td><td>'.$value['prix'].'</td><td>'.$value['description'].'</td><td>'.$value['stock'].'</td></tr>';
            }
            echo '</tbody></table>';
            echo '<br/><br/>';
        }

        ////////////////////////////////////Get all categories///////////////////////////////////////////////////////////
        $functionGetCategories = 'getCategories';
        $categories = $soapClient->__soapcall($functionGetCategories,[]);
        echo '<p> Veuillez sélectioner la catégorie qui vous intéresse : </p>';
        echo '<form action="http://localhost/client.php" method="post">
            <select name="categorie">';
            foreach ($categories as $key => $value) {
                echo '<option value="'.$value['id'].'">'.$value['nom'].'</option>';
            }
        echo '</select>
            <input type="submit" value="Valider" />
            </form>';

        echo '<br/>';

        ////////////////////////////////////Modify article///////////////////////////////////////////////////////////
        echo '<h1><b>Changer un champ des articles  : </b></h1><br/>';
        echo '<p> Une description ou autre ne vous convient pas dans nos articles conseillées ? Faites vos changements ci-dessous ! </p>';
        foreach ($bestSell as $key => $value) {
            echo '<form action="http://localhost/client.php" method="post">';
            echo '<input id="id'.$value['id'].'" name="id" type="hidden" value="'.$value['id'].'">';
            echo '<h3>'.$value['nom'].'</h3>';
            echo '<label>Nom </label>';
            echo '<input type="text" name="nom" placeholder="'.$value['nom'].'"/>';
            echo '<label>Prix </label>';
            echo '<input type="number" name="prix" min="0" step="0.01" placeholder="'.$value['prix'].'"/>';
            echo '<label>Description </label>';
            echo '<textarea name="description" placeholder="'.$value['description'].'"></textarea>';
            echo '<label>Stock </label>';
            echo '<input type="number" name="stock" min="0" step="1" placeholder="'.$value['stock'].'"/>';
            echo '<input type="submit" value="Valider" />
            </form>';
        }



        //For DEBUG
        //$functions = $soapClient->__getFunctions ();
        //var_dump ($functions);
    }catch(SoapFault $fault){

        // <xmp> tag displays xml output in html 
        echo 'Request : <br/><xmp>', 
        $soapClient->__getLastRequest(), 
        '</xmp><br/><br/> Error Message : <br/>', 
        $fault->getMessage(); 
    }
?>