-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 05 juin 2019 à 09:21
-- Version du serveur :  10.1.35-MariaDB
-- Version de PHP :  7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mi03`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `prix` decimal(10,2) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `categorie_id`, `nom`, `prix`, `description`, `stock`) VALUES
(1, 1, 'Marteau', '10.00', 'Marteau, maillet, massette pas cher. Grand choix, promos permanentes et livraison rapide partout en France. Paiement sécurisé.', 100),
(2, 2, 'Cacahuete enrobe ', '5.00', 'Cacahuètes enrobées caramélisées. Retrouvez le goût des \"pralines\" ou \"chouchous\" des vacances ! ', 100),
(3, 1, 'Masse', '15.00', 'Une massette et une masse désignent cet outil ancestral qu\'est une pierre au bout d\'un bâton.', 100),
(4, 2, 'Cacao', '5.00', 'Le cacao est la poudre obtenue après broyage de l\'amande des fèves de cacao plates fermentées produites par le cacaoyer. Cette opération permet également d\'extraire la graisse que l\'on appelle beurre de cacao. La pâte de cacao est quant à elle produite à partir des fèves de cacao ', 100),
(5, 3, 'sabre laser', '100.00', 'Un sabre laser est une arme de la saga cinématographique Star Wars. Il s’agit du symbole le plus fort de la puissance d’un Jedi.', 100),
(6, 3, 'boite mystere metallique super mario', '25.00', 'Rangez toutes vos affaires précieuses dans cette boîte mystère à l’effigie du jeu Super Mario Bros ! L’univers Nintendo pixélisé est fièrement représenté sur cette boîte à trésor métallique hautement geek', 25);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Outils'),
(2, 'Nourriture'),
(3, 'Technologie');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `mail`, `mdp`) VALUES
(1, 'Debbouze', 'debbouze@mail.fr', '123456789'),
(2, 'Bouvarel', 'bouvarel@mail.fr', '123456789'),
(3, 'Martin', 'martin@mail.fr', '123456'),
(4, 'Coat', 'coat@mail.fr', '123456'),
(5, 'didi', 'didi', '$2y$12$dVSP2TJtFnTD5zEMFbVDRO47n8MwL4C9qGFzNgT0zVMIwmXn5pOA.'),
(6, 'ade', 'ade', '$2y$12$RYaMyUAGdehJWbbC2FJx7eJ5bjsBq6iyFaE8sKIbLUkXvxmCUoA4i');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `client_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `date`, `etat`, `client_id`) VALUES
(1, '2019-02-22', 0, 1),
(2, '2019-02-22', 0, 4),
(3, '2019-02-21', 0, 2),
(4, '2019-02-21', 1, 3),
(5, '2019-03-15', 1, 1),
(6, '2019-04-03', 1, 5),
(7, '2019-04-03', 1, 5),
(8, '2019-04-03', 1, 5),
(9, '2019-04-10', 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `lignecommande`
--

CREATE TABLE `lignecommande` (
  `article_id` int(11) NOT NULL,
  `commande_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `prix` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `lignecommande`
--

INSERT INTO `lignecommande` (`article_id`, `commande_id`, `quantite`, `prix`) VALUES
(1, 3, 1, '10.00'),
(1, 6, 1, '10.00'),
(1, 7, 1, '10.00'),
(1, 8, 1, '10.00'),
(1, 9, 8, '80.00'),
(2, 1, 1, '5.00'),
(2, 5, 1, '5.00'),
(2, 6, 1, '5.00'),
(2, 7, 1, '5.00'),
(2, 8, 1, '5.00'),
(3, 2, 1, '10.00'),
(5, 4, 2, '100.00'),
(6, 1, 2, '25.00'),
(6, 4, 1, '25.00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_23A0E66BCF5E72D` (`categorie_id`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6EEAA67D19EB6921` (`client_id`);

--
-- Index pour la table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD PRIMARY KEY (`article_id`,`commande_id`),
  ADD KEY `IDX_853B79397294869C` (`article_id`),
  ADD KEY `IDX_853B793982EA2E54` (`commande_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E66BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `FK_6EEAA67D19EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD CONSTRAINT `FK_853B79397294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  ADD CONSTRAINT `FK_853B793982EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `commande` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
